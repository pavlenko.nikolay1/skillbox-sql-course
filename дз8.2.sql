select to_char(sum(salary), '999,999.99C') as Sum_salary from employees;
--
select trunc(hire_date, 'yy') from employees;
--
select to_char(trunc(hire_date, 'yy'), 'yyyy') as txt_year from employees;
--
with num_year as 
(select to_number(to_char(trunc(hire_date, 'yy'), 'yyyy'), '9999') as num_year from employees)
--
select min(num_year), max(num_year), round(avg(num_year), 2) from num_year;
--
create table hurricane(name          varchar2(64),
                       report_year   date,
                       victims       number);
--
alter table hurricane modify name not null;
--
truncate table hurricane;
--
drop table hurricane;
